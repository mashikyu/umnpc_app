import { DailyQuizService } from './../service/QuizService';
import { EventModalCalenderPage } from './../pages/event-modal-calender/event-modal-calender';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { RegisterPage } from '../pages/register/register';
import { TimelinePage } from '../pages/timeline/timeline';
import { DailyQuizPage } from '../pages/daily-quiz/daily-quiz';
import { DailyProblemPage } from '../pages/daily-problem/daily-problem';
import { ChatPage } from '../pages/chat/chat';
import { SettingsPage } from '../pages/settings/settings';
import { ListMemberPage } from '../pages/list-member/list-member';
import { ListCommitteePage } from '../pages/list-committee/list-committee';
import { LeaderboardPage } from '../pages/leaderboard/leaderboard';
import { EventPage } from '../pages/event/event';
import { CalendarPage } from '../pages/calendar/calendar';
import { VerifyUserPage } from '../pages/verify-user/verify-user';
import { TimelineDetailPage } from '../pages/timeline-detail/timeline-detail';
import { AdminPage } from '../pages/admin/admin';
import { ViewMemberPage } from '../pages/view-member/view-member';
import { ViewComiteePage } from '../pages/view-comitee/view-comitee';
import { SettingsPasswordPage } from '../pages/settings-password/settings-password';
import { AddSoalPage } from '../pages/add-soal/add-soal';

import { AddNotePage } from '../pages/add-note/add-note';
import { DetailsPage } from '../pages/details/details';
import { NoteService } from '../service/NoteService';
import { CommentPage } from '../pages/comment/comment';
import { CommentService } from '../service/CommentService';
import { CommentlistPage } from '../pages/commentlist/commentlist';
import { ChatroomPage } from '../pages/chatroom/chatroom';
import { AdminDailyProblemPage } from '../pages/admin-daily-problem/admin-daily-problem';
import { AdminResetProblemPage } from '../pages/admin-reset-problem/admin-reset-problem';
import { AddeventpagePage } from '../pages/addeventpage/addeventpage';
import { EventdetailsPage } from '../pages/eventdetails/eventdetails';

import { AuthService } from '../service/AuthService';
import { UserService } from '../service/UserService';
import { MiscService } from '../service/MiscService';
import { TimelineService } from '../service/TimelineService';
import { QuestionService } from '../service/QuestionService';
import { EventService } from '../service/EventService';
import { DailyProblemService } from '../service/DailyProblemService';
import {  NgCalendarModule} from 'ionic2-calendar';

import {DatePipe} from '@angular/common';

import{LocalNotifications} from '@ionic-native/local-notifications';
import { ImagePicker } from '@ionic-native/image-picker';
import { Base64 } from '@ionic-native/base64';

import { HttpModule } from '@angular/http';

var config = {
  apiKey: "AIzaSyC7liL1RRqGWAsy1-YcMrwfKfOjZmSNmck",
  authDomain: "umnpc-app.firebaseapp.com",
  databaseURL: "https://umnpc-app.firebaseio.com",
  projectId: "umnpc-app",
  storageBucket: "umnpc-app.appspot.com",
  messagingSenderId: "851226009946"
};

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    RegisterPage,
    TimelinePage,
    DailyQuizPage,
    DailyProblemPage,
    ChatPage,
    SettingsPage,
    ListMemberPage,
    ListCommitteePage,
    LeaderboardPage,
    EventPage,
    CalendarPage,
    VerifyUserPage,
    TimelineDetailPage,
    AdminPage,
    ViewMemberPage,
    ViewComiteePage,
    AddSoalPage,
    AdminDailyProblemPage,
    AdminResetProblemPage,

    SettingsPasswordPage,
    EventModalCalenderPage,

    AddNotePage,
    DetailsPage,
    CommentPage,
    CommentlistPage,
    ChatroomPage,
    AddeventpagePage,
    EventdetailsPage
  ],
  imports: [
    BrowserModule,
    AngularFireDatabaseModule,
    AngularFireModule.initializeApp(config),
    NgCalendarModule,
    HttpModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    RegisterPage,
    TimelinePage,
    DailyQuizPage,
    DailyProblemPage,
    ChatPage,
    SettingsPage,
    ListMemberPage,
    ListCommitteePage,
    LeaderboardPage,
    EventPage,
    CalendarPage,
    VerifyUserPage,
    TimelineDetailPage,
    AdminPage,
    ViewMemberPage,
    ViewComiteePage,
    AddSoalPage,
    AdminDailyProblemPage,
    AdminResetProblemPage,
    
    SettingsPasswordPage,
    EventModalCalenderPage,

    AddNotePage,
    DetailsPage,
    CommentPage,
    CommentlistPage,
    ChatroomPage,
    AddeventpagePage,
    EventdetailsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {
      provide: ErrorHandler, 
      useClass: IonicErrorHandler
    },
    AuthService,
    UserService,
    MiscService,
    TimelineService,
    EventService,
    QuestionService,
    NoteService,
    CommentService,
    LocalNotifications,
    DailyProblemService,
    DatePipe,
    DailyQuizService,
    ImagePicker,
    Base64,
  ]
})
export class AppModule {}
