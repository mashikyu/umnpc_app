export interface Note{
    id: string,
    author : string,
    title : string,
    text : string,
}