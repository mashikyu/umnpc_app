import { AngularFireDatabase } from 'angularfire2/database';
import firebase from 'firebase';

export class UserService 
{
    registered_users = [];
    members = [];
    committees = [];    

    load_all_registered_users()
    {
        firebase.database().ref('registered_users').on('value', (snapshot) => {
            this.registered_users = [];
            let result = snapshot.val();
            for(let k in result) {
                this.registered_users.push({
                    id: k,
                    first_name: result[k].first_name,
                    last_name: result[k].last_name,
                    email: result[k].email,
                    cf_handle: result[k].cf_handle,
                    password: result[k].password,
                    score:result[k].score
                });
            }
        });
    }
    
    load_all_members()
    {
        firebase.database().ref('pc_users').on('value', (snapshot) => {
            this.members = [];
            let result = snapshot.val();
            for(let k in result) {
                if(result[k].status === 0)
                {
                    this.members.push({
                        id: k,
                        first_name: result[k].first_name,
                        last_name: result[k].last_name,
                        email: result[k].email,
                        cf_handle: result[k],
                        password: result[k].password,
                        score: result[k].score,
                        status: result[k].status,
                        random_chance: result[k].random_chance,
                        daily_problem: result[k].daily_problem,
                    });
                }
            }
        });
    }
    

    get_user_status(id:string){
        let result;
        firebase.database().ref('pc_users/'+id).once("value", (snapshot) => {
            result = snapshot.val();
         //   ////console.log(result.random_chance + " mom");
            return result.status;
        });
        return result.status;
    }
    load_all_committees()
    {
        firebase.database().ref('pc_users').on('value', (snapshot) => {
            this.committees = [];
            let result = snapshot.val();
            for(let k in result) {
                if(result[k].status === 1)
                {
                    this.committees.push({
                        id: k,
                        first_name: result[k].first_name,
                        last_name: result[k].last_name,
                        email: result[k].email,
                        cf_handle: result[k],
                        password: result[k].password
                    });
                }
            }
        });
    }

    get_user_score(id:String)
    {
        let result;
        firebase.database().ref('pc_users/'+id).once("value", (snapshot) => {
            result = snapshot.val();
           // //////console.log(result.random_chance + " mom");
            return result.score;
        });
        return result.score;
    }

    update_user_score(id:string,new_score:string)
    {
        firebase.database().ref('pc_users/' + id).update(
            { 
                score:new_score
            }
        );
    }

    

    update_user_random_chance(id:string,new_chances:string)
    {
        firebase.database().ref('pc_users/' + id).update(
            { 
                random_chance:new_chances
            }
        );
    }

    register(fname: string, lname: string, mail: string, cfHandle: string, pass: string) 
    {
        var new_registered_user = {
            first_name : fname,
            last_name : lname,
            email : mail,
            cf_handle : cfHandle,
            password : pass
        };

        firebase.database().ref('registered_users').push(new_registered_user);
        this.load_all_registered_users();
    }

    ///
   

    update_new_score(id:string,new_score:string)
    {
        firebase.database().ref('pc_users/' + id).update(
            { 
                score:new_score
            }
        );
    }

    get_user_randomChance(id:String)
    {
        let result;
        firebase.database().ref('pc_users/'+id).once("value", (snapshot) => {
             result = snapshot.val();
            ////console.log(result.random_chance + " mom");
            return result.random_chance;
        });
        return result.random_chance;
    }

    update_user_randomChance(id:string,new_chances:string)
    {
        firebase.database().ref('pc_users/' + id).update(
            { 
                random_chance: new_chances
            }
        );
    }
    ///
    get_current_user_id_byemail(email:string){
        var id;
        ////console.log("USER SERVICE HERE");
        firebase.database().ref('pc_users').once('value', (snapshot) => {
            let result = snapshot.val();
            ////console.log("GET CURRENT ID");
            for(let k in result) {
                ////console.log(result[k].email + " " + k);
                if(email == result[k].email) {
                    id = k;
                    ////console.log("YES " + k);
                    break;
                }
                //else ////console.log('NOOO');
            }
        });
        return id;
    }
    ///
    add_user(user)
    {
        var new_user = {
            first_name : user.first_name,
            last_name : user.last_name,
            email : user.email,
            cf_handle : user.cf_handle,
            password : user.password,
            status: '0',
            random_chance: '3',
            score: '100',
            daily_problem:'11A'
        };

        firebase.database().ref('pc_users/').push(new_user);
        this.delete_registered_user(user.id);
        this.load_all_members();
    }

    show_all_registered_users() 
    {
        return this.registered_users;
    }

    show_all_committee()
    {
        return this.committees;
    }

    show_all_member()
    {
        return this.members;
    }

    get_total_member() 
    {
        return this.members.length;
    }

    delete_registered_user(id: string) 
    {
        firebase.database().ref('registered_users/' + id).remove();
        this.load_all_registered_users();
    }

    delete_user(id: string)
    {
        firebase.database().ref('pc_users/' + id).remove();
    }

    update_user(user)
    {
        firebase.database().ref('pc_users/' + user.id).update(
            { 
                first_name: user.first_name, 
                last_name: user.last_name,
                cf_handle : user.cf_handle
            }
        );
    }

    reload_random_chance()
    {
        firebase.database().ref('pc_users').once('value', (snapshot) => {
            let result = snapshot.val();
            for(let k in result) {
                firebase.database().ref("pc_users/" + result[k].id).update({ random_chance: 3});
            }
        });
    }
    
    get_current_user_from_email(email: string)
    {
        var user;
        ////console.log("USER SERVICE HERE");
        firebase.database().ref('pc_users').once('value', (snapshot) => {
            let result = snapshot.val();
            for(let k in result) {
                if(result[k].email === email) {
                    user = result[k];
                    break;
                }
            }
        });

        return user;
    }

    to_committee(id: string) 
    {
        ////console.log("TO COMMITTEE!");
        ////console.log(id);
        firebase.database().ref("pc_users/" + id).update({
            status : 1
        });
    }

    to_member(id: string)
    {
        firebase.database().ref("pc_users/" + id).update({
            status : 0
        });
    }

    get_user_username(id:string){
        let result;
        firebase.database().ref('pc_users/'+id).once("value", (snapshot) => {
            result = snapshot.val();
            ////console.log(result.random_chance + " mom");
            return result.first_name + " " + result.last_name;
        });
        return result.first_name + " " + result.last_name;
    }

    encodeImageUri(imageUri, callback) {
        var c = document.createElement('canvas');
        var ctx = c.getContext("2d");
        var img = new Image();
        img.onload = function () {
          var aux:any = this;
          c.width = aux.width;
          c.height = aux.height;
          ctx.drawImage(img, 0, 0);
          var dataURL = c.toDataURL("image/jpeg");
          callback(dataURL);
        };
        img.src = imageUri;
      };

      update_user_status(id:string,new_status:string)
    {
        firebase.database().ref('pc_users/' + id).update(
            { 
                status:new_status
            }
        );
    }
    uploadImage(imageURI){
        // return new Promise<any>((resolve, reject) => {
        //   let storageRef = firebase.storage().ref();
        //   let imageRef = storageRef.child('image').child('imageName');
        //   this.encodeImageUri(imageURI, function(image64){
        //     imageRef.putString(image64, 'data_url')
        //     .then(snapshot => {
        //       resolve(snapshot.downloadURL)
        //     }, err => {
        //       reject(err);
        //     })
        //   })
        // })

        var new_image = {
            
            image:imageURI,
        };

        firebase.database().ref('pc_image/').push(new_image);
        
      }
}
