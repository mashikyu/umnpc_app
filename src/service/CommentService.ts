import { Comment } from "../data/comments";
import firebase from 'firebase';

export class CommentService{
    private allComment = [];

    load_all_comment(){
        this.allComment = [];
        firebase.database().ref('pc_comment').on('value', (snapshot) => {
            let result = snapshot.val();
            for(let k in result) {
                    this.allComment.push({
                        id: k,
                        id_post : result[k].id_post,
                        username: result[k].username,
                        content: result[k].content,
                        time: result[k].time,
                        // comment_counter: result[k].comment_counter
                    });
                
            }
        });
    }

    show_comment(id){
        var comment = [];
        for(var i = 0; i<this.allComment.length ; i++){
            if(id === this.allComment[i].id_post){
                comment.push({
                    id: this.allComment[i].id,
                    id_post : this.allComment[i].id_post,
                    username: this.allComment[i].username,
                    content: this.allComment[i].content,
                    time: this.allComment[i].time,
                })
            }
        }

        return comment;
    }

    getCommentLength(id){
        var comment = [];
        for(var i = 0; i<this.allComment.length ; i++){
            if(id === this.allComment[i].id_post){
                comment.push({
                    id: this.allComment[i].id,
                    id_post : this.allComment[i].id_post,
                    username: this.allComment[i].username,
                    content: this.allComment[i].content,
                })
            }
        }

        return comment.length;
    }


    addComment(id_post: string, username: string, newContent: string, time:string){
        // let newComm : Comment = {id_post:id_post, id_user:id_user, content:newContent};

        var new_comment = {
            id_post:id_post, 
            username:username, 
            content:newContent,
            time: time,
          };
      
          firebase.database().ref('pc_comment').push(new_comment);

        this.load_all_comment();
    }

    delete_comment(id: string, id_post) 
    {
        for(var i = 0; i<this.allComment.length ; i++){
            if(id_post === this.allComment[i].id_post){
                firebase.database().ref('pc_comment/' + id).remove();        
            }
        }
        this.load_all_comment();
    }

    get_current_comment_by_id_post(id_post:string){
        var id;
        console.log("COMMENT HERE");
        firebase.database().ref('pc_comment').once('value', (snapshot) => {
            let result = snapshot.val();
            console.log("GET CURRENT ID");
            for(let k in result) {
                console.log(result[k].id_post + " " + k);
                if(id_post == result[k].id_post) {
                    id = k;
                    console.log("YES " + k);
                    break;
                }
                else console.log('NOOO');
            }
        });
        return id;
    }
}