import { AngularFireDatabase } from 'angularfire2/database';
import {Injectable} from "@angular/core";
import firebase from 'firebase';
import { DAILYPROBLEM } from './mock-daily-problem';

@Injectable()
export class DailyProblemService
{
    private dailyproblem: any;

    constructor() {
      this.dailyproblem = DAILYPROBLEM;
    }
  
    getAll() {
      return this.dailyproblem;
    }

    get_current_daily_problem(id: string)
    {
        let current_daily_problem;
        firebase.database().ref('pc_users/' + id).on("value", (snapshot) => {
            let result = snapshot.val();
            current_daily_problem = result.daily_problem;
        });
        
        return current_daily_problem;
    }

    update_user_daily_problem(id:string,problem:string)
    {
        firebase.database().ref('pc_users/' + id).update(
            { 
                daily_problem:problem
            }
        );
    }

    
}