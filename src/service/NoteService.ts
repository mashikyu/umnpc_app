import { Note } from "../data/notes";

export class NoteService{
    private allNotes: Note[] = [];

    addNotes(newId: string, newTitle:string, newAuthor: string, newText: string){
        let newNote : Note = {id: newId, title:newTitle, author:newAuthor, text:newText};

        this.allNotes.push(newNote);
    }

    removeNotes(note: Note){
        if(this.allNotes.indexOf(note)>-1){
            this.allNotes.splice(this.allNotes.indexOf(note),1);
        }
    }

    fetchAllNotes(){
        return this.allNotes;
    }

    addindex(){
        let newI
    }

    // getNoteLength(){
    //     return this.allNotes.length;
    // }
}