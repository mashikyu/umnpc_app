import firebase from 'firebase';

export class AuthService 
{
    register(email: string, password: string) {
        console.log(email + " " + password);
        return firebase.auth().createUserWithEmailAndPassword(email,password);
    }

    login(email:string, password: string) {
        firebase.auth().signInWithEmailAndPassword(email, password)
            .catch(function(error) {
                var errorCode = error.code;
                var errorMessage = error.message;
                if(errorCode == 'auth/wrong-password') {
                    alert('Wrong Password');
                } 
                else if(errorCode == 'auth/invalid-email') {
                    alert('Invalid Email');
                }
                else if(errorCode == 'auth/user-disabled') {
                    alert('User Disabled');
                }
                else if(errorCode == 'auth/user-not-found') {
                    alert('User Not Found');
                }
                else {
                    alert(errorMessage);
                }
            });
        return;
    }

    logout() {
        firebase.auth().signOut();
    }
    
    getCurrentUser(){
        let cu = firebase.auth().currentUser;
        return cu;
    }
    
    getCurrentUserEmail(){
        let cu = firebase.auth().currentUser.email;
        return cu;
    }

    
}