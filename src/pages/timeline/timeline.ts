import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, Toast } from 'ionic-angular';
import { TimelineService } from "../../service/TimelineService";
import { TimelineDetailPage } from "../timeline-detail/timeline-detail";

import { AuthService } from '../../service/AuthService';
import { UserService } from '../../service/UserService';
import { User } from '../../service/user.interface';

import { AlertController, ModalController, ToastController } from 'ionic-angular';
import { NoteService } from '../../service/NoteService';
import { AddNotePage } from '../add-note/add-note';
import { DetailsPage } from '../details/details';
import { Note } from '../../data/notes';
import { CommentPage } from '../comment/comment';
import { CommentlistPage } from '../commentlist/commentlist'
import { CommentService } from '../../service/CommentService';
import { AngularFireDatabase } from "angularfire2/database";
import firebase from 'firebase';

/**
 * Generated class for the TimelinePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-timeline',
  templateUrl: 'timeline.html',
  // providers: [NoteService],
})

export class TimelinePage
{
//  users : Map<string, Object>;
  allNotes = [];
  commentLength: any;
  postid: any[] = [];
 

  constructor(public db:AngularFireDatabase, public navCtrl: NavController, public navParams: NavParams, public authService: AuthService, public timelineService: TimelineService, 
              public userService: UserService, public modalCtrl: ModalController, public noteService: NoteService, public toastCtrl: ToastController,
              public commentService: CommentService, public alertCtrl: AlertController) 
  {
    this.allNotes = this.timelineService.show_all_post();
    // _.sortBy(this.allNotes).reverse();
    console.log(this.allNotes);
  }

  ngOnInit(){
    // var fbkey = new this.firebase("https://umnpc-app.firebaseio.com/pc_post");
    // var fbname = fbkey.orderByChild("");
    // let LessonSets = fbkey;
    // let lessonSetsArray = [];
    // fbkey.forEach(postkey => {
    //   postkey.forEach(OneLessonSet => {
    //     lessonSetsArray.push(OneLessonSet);
    //   });
    // });
  //   firebase.database().ref('pc_post').on('value', (snapshot) => {
  //     let result = snapshot.val();
  //     for(let k in result) {
  //         this.allNotes.push({
  //             id : k,
  //             author: result[k].id_user,
  //             text: result[k].statement,
  //             // time: result[k].time,
  //             title: result[k].like_counter,
  //             // comment_counter: result[k].comment_counter
  //         });
  //     }
  // });
    // this.timelineService.load_all_posts();
    // this.timelineService.show_all_posts();
    // this.timelineService.get_post();
    console.log(this.allNotes);
  }

  ionViewDidLoad() 
  {
    // this.users = this.userService.show_all_member();
    // this.commentLength = this.commentService.getCommentLength();
    console.log(this.allNotes);
  }

  ionViewWillEnter(){
    // this.commentLength = this.commentService.getCommentLength
    // this.allNotes = this.timelineService.show_all_post();
  }


  viewDetail(note) 
  {
    this.navCtrl.push(TimelineDetailPage, {note: note});
  // submit_logout(){
  //   this.authService.logout();
  // }
  }

  openAddModal(){
    let modal = this.modalCtrl.create(AddNotePage);
    modal.onDidDismiss(()=>{
      this.allNotes = this.timelineService.show_all_post();
    })

    modal.present();
  }

  openModalDetail(note){
    this.navCtrl.push(DetailsPage, note);
  }

  openModalDelete(note){

    const alert = this.alertCtrl.create({
      title: 'Delete Post',
      message: 'Are you sure want to delete this post?',
      cssClass: 'custom-alertDanger',
      buttons: [
        {
          text: 'Yes',
          handler: () => {
            this.timelineService.delete_post(note.id);
            this.allNotes = this.timelineService.show_all_post();
            console.log(this.allNotes);

            let toast = this.toastCtrl.create({
              message: 'Post has been deleted',
              duration: 2000,
              position: 'bottom'
            });
          
            toast.present();
          }
        },
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'custom-alertDanger',
          handler: () => {
            console.log("Cancel is clicked!");
          }
        }
      ]
    });

    alert.present();
  }

  openCommentlistModal(note){
    let modal = this.modalCtrl.create(CommentlistPage);

    modal.present();
  }

  addLike(){
    console.log("Increase like");
  }

  is_adminss(){
    let user_now = this.authService.getCurrentUser();

    if(user_now == null) return false;
    if(user_now.email == "umnpc@umnpc.com") return true;
    return false;
  }
}
