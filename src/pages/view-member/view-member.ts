import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { UserService } from '../../service/UserService';

/**
 * Generated class for the ViewMemberPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-view-member',
  templateUrl: 'view-member.html',
})
export class ViewMemberPage {

  users = [];
  
  constructor(public navCtrl: NavController, public navParams: NavParams, public userService: UserService,
              public alertCtrl: AlertController) {
    this.users = this.userService.show_all_member();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ViewMemberPage');
  }

  popPage(){
    this.navCtrl.pop();
  }

  onShowAlertAngkatPengurus(user){
    const alert = this.alertCtrl.create({
      title: 'Change Status to Committee',
      message: 'Are you sure to add ' +  user.first_name + ' ' + user.last_name + ' to committee?<br/>',
      buttons: [
        {
          text: 'Yes',
          handler: () => {
            //ganti status user
            this.userService.to_committee(user.id);
            this.users = this.userService.show_all_member();
            console.log("UPDATE STATUS SUCCESSFUL");
          }
        },
        {
          text: 'No',
          role: 'cancel',
          handler: () => {
            console.log("Cancel is clicked!");
          }
        }
      ]
    });

    alert.present();
  }

  onShowAlertDelete(user){
    const alert = this.alertCtrl.create({
      title: 'Delete User',
      message: 'Are you sure want to delete this user? <br/>' +
              'Name: ' + user.first_name + ' ' + user.last_name + '<br/>' + 
              ' Email: ' + user.email + '<br/>',
      buttons: [
        {
          text: 'Ya',
          handler: () => {
            //delete user
            this.userService.delete_user(user.id);
            this.users = this.userService.show_all_member();
          }
        },
        {
          text: 'Tidak',
          role: 'cancel',
          handler: () => {
            console.log("Cancel is clicked");
          }
        }
      ]
    });

    alert.present();
  }

}
