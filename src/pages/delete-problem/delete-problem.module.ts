import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DeleteProblemPage } from './delete-problem';

@NgModule({
  declarations: [
    DeleteProblemPage,
  ],
  imports: [
    IonicPageModule.forChild(DeleteProblemPage),
  ],
})
export class DeleteProblemPageModule {}
