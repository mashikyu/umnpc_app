import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ModalOptions, ToastController } from 'ionic-angular';
import { SettingsPasswordPage } from '../settings-password/settings-password';
import { UserService } from '../../service/UserService';
import { AuthService } from '../../service/AuthService';
import { User } from '../../data/user.interface';
import { ImagePicker } from '@ionic-native/image-picker';
import { normalizeURL } from 'ionic-angular';
import { Base64 } from '@ionic-native/base64';
/**
 * Generated class for the SettingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {
  current_user:User[] = [];
  public isEdit : boolean;

  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController, 
              public userService: UserService, public authService: AuthService, public imagePicker: ImagePicker,
              public toastCtrl: ToastController, public base64: Base64) 
  {
    let current_email = this.authService.getCurrentUser().email;
    this.current_user = this.userService.get_current_user_from_email(current_email);
  }

  myModalOption: ModalOptions = {
    enableBackdropDismiss: false
  };

  ionViewDidLoad() {
    console.log('ionViewDidLoad SettingsPage');
    this.isEdit=false;
  }

  edit_account () {
    this.isEdit=!this.isEdit;    
  }

  account_setting(){
    this.isEdit=!this.isEdit;
  }

  change_password (){
    this.navCtrl.push(SettingsPasswordPage);
  }

  openModal (){
    const myModal = this.modalCtrl.create(SettingsPasswordPage);

    myModal.present();
  }

  // changePass () {
  //   let profileModal = this.modalCtrl.create()
  // }

  uploadImageToFirebase(image){
    // image = normalizeURL(image);
  
    //uploads img to firebase storage
    this.userService.uploadImage(image)
    // .then(photoURL => {
  
    //   let toast = this.toastCtrl.create({
    //     message: 'Image was updated successfully' + photoURL,
    //     duration: 3000
    //   });
    //   toast.present();
    //   })
    }

  openImagePicker(){
    this.imagePicker.hasReadPermission().then(
      (result) => {
        if(result == false){
          // no callbacks required as this opens a popup which returns async
          this.imagePicker.requestReadPermission();
        }
        else if(result == true){
          this.imagePicker.getPictures({
            maximumImagesCount: 1
          }).then(
            (results) => {
              for (var i = 0; i < results.length; i++) {
                // this.uploadImageToFirebase(results[i]);
                this.base64.encodeFile(results[i]).then((base64File: string) => {
                  this.uploadImageToFirebase(base64File);
                })
              }
            }, (err) =>{
              console.log(err);
              let toast = this.toastCtrl.create({
                message: 'Fail update image',
                duration: 3000
              });
              toast.present();
            } 
          );
        }
      }, (err) => {
        console.log(err);
      });
    }
}
