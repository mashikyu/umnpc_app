import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VerifyUserPage } from './verify-user';

@NgModule({
  declarations: [
    VerifyUserPage,
  ],
  imports: [
    IonicPageModule.forChild(VerifyUserPage),
  ],
})
export class VerifyUserPageModule {}
