import { quiz } from './../../data/quiz';

import { AuthService } from './../../service/AuthService';
import { UserService } from './../../service/UserService';
import { DailyQuizService } from './../../service/QuizService';
import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LeaderboardPage } from '../leaderboard/leaderboard';

@IonicPage()
@Component({
  selector: 'page-daily-quiz',
  templateUrl: 'daily-quiz.html',
})
export class DailyQuizPage  implements OnInit{
  
  i:any;
  length:any;

  soal:quiz;
  kumpulanQuiz=[];
  
  userId:string;

  jawabanUser = [];
  constructor(public navCtrl: NavController, public navParams: NavParams,public dailyQuizServ:DailyQuizService,public userSrv:UserService,public authSrv:AuthService) {
  
  
  }
  sudahdikerjain:string;
  sudah:any;
  r:any;
  ngOnInit(){
    let uemail=this.authSrv.getCurrentUserEmail();
    this.userId=this.userSrv.get_current_user_id_byemail(uemail);
    this.r=this.userSrv.get_user_status(this.userId);

     if(this.r!=1&&this.r=='1'){
      console.log('doggg');
      let soalQuiz=[];
      soalQuiz=this.dailyQuizServ.get_all_quiz();
      console.log(soalQuiz);
     
      this.kumpulanQuiz=soalQuiz;
     }
    else this.sudah='please wait for next one ';
  }

  is_adminss(){
    
    if(this.r!=1&&this.r=='1') return true;
    return false;
  }

  

  jawabQuiz(value,i){
    this.jawabanUser[i]=value;
    
    console.log(this.jawabanUser[i]);

  }
  
  checkAnswer(){
    for(let x=0;x<this.dailyQuizServ.get_length_soal();x++)
      {
      if(this.jawabanUser[x]==this.kumpulanQuiz[x].jawaban)
      {

        let r=this.userSrv.get_user_score(this.userId)+200;
        this.userSrv.update_user_score(this.userId,r);
        this.userSrv.update_user_status(this.userId,'1');
        this.sudahdikerjain='true';
        this.navCtrl.push(LeaderboardPage);
      }
    }
  }
}
