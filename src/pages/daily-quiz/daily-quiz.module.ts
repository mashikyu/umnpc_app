import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DailyQuizPage } from './daily-quiz';

@NgModule({
  declarations: [
    DailyQuizPage,
  ],
  imports: [
    IonicPageModule.forChild(DailyQuizPage),
  ],
})
export class DailyQuizPageModule {}
