import { AuthService } from './../../service/AuthService';
import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { DailyProblemService } from '../../service/DailyProblemService';
import { UserService } from '../../service/UserService';



@IonicPage()
@Component({
  selector: 'page-admin-daily-problem',
  templateUrl: 'admin-daily-problem.html',
})

export class AdminDailyProblemPage  implements OnInit{

 

  constructor(public navCtrl: NavController, public navParams: NavParams, public dailyProblemService: DailyProblemService,public alertCtrl: AlertController,public userServ:UserService,public autSrv:AuthService) {
    
  }
 // member:[];
  solvedkah:string;
  buatfor: object[]=[];
  memberId=[];
  memberCf=[];
  trueCF=[];
  memberName=[];
  memberDailyProblem=[];
  ngOnInit(){
    let id=[];
    let members=[];
    this.userServ.load_all_members();
    members=this.userServ.show_all_member();
    this.buatfor=members;
    console.log("this.memberId");
    console.log(this.buatfor);
    for(let x=0;x<this.userServ.get_total_member();x++)
    {
        this.memberId.push(members[x].id);
        this.memberCf.push(members[x].email);
        console.log('hello');
        console.log(this.memberCf);
        console.log('hello');
        this.memberDailyProblem.push(members[x].daily_problem);
        this.memberName.push(members[x].first_name+' '+members[x].last_name);
    }
    console.log(this.memberId);
  console.log("this.memberId");
    console.log(this.memberCf);
     console.log("this.memberId");
    console.log(this.memberDailyProblem);
    

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AdminDailyProblemPage');
  }

  benar(emailUser:any){
    
    let members=[];
    this.userServ.load_all_members();
    members=this.userServ.show_all_member();
    const alert = this.alertCtrl.create({
      title: 'Verify Answer',
      message: 'Are you sure this problem answer is true?',
      buttons: [
        {
          text: 'Yes',
          handler: () => {
            ////
            
            for(let x=0;x<this.userServ.get_total_member();x++)
              {
              if(members[x].email==emailUser)
                {
                console.log('done');
                let winnerId =this.userServ.get_current_user_id_byemail(emailUser);
                let x= this.userServ.get_user_score(winnerId);
                let cx=x+100;
                this.userServ.update_new_score(winnerId,cx);
                x= this.userServ.get_user_score(winnerId);
                console.log(this.userServ.get_current_user_id_byemail(this.autSrv.getCurrentUserEmail()));
                console.log('x=='+x);
                }  
              }
            ////
          }
        },
        {
          text: 'No',
          role: 'cancel',
          handler: () => {
            console.log("Cancel is clicked");
          }
        }
      ]
    });

    alert.present();
  }

  salah(){
    const alert = this.alertCtrl.create({
      title: 'Verify Answer',
      message: 'Are you sure this problem answer is false?',
      buttons: [
        {
          text: 'Yes',
          handler: () => {
            //do something
          }
        },
        {
          text: 'No',
          role: 'cancel',
          handler: () => {
            console.log("Cancel is clicked");
          }
        }
      ]
    });

    alert.present();
  }

}
