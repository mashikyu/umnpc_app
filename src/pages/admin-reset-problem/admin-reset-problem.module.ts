import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdminResetProblemPage } from './admin-reset-problem';

@NgModule({
  declarations: [
    AdminResetProblemPage,
  ],
  imports: [
    IonicPageModule.forChild(AdminResetProblemPage),
  ],
})
export class AdminResetProblemPageModule {}
