import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EventModalCalenderPage } from './event-modal-calender';

@NgModule({
  declarations: [
    EventModalCalenderPage,
  ],
  imports: [
    IonicPageModule.forChild(EventModalCalenderPage),
  ],
})
export class EventModalCalenderPageModule {}
