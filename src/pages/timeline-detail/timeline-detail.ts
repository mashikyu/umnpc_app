import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { TimelineService } from "../../service/TimelineService";
import { CommentService } from '../../service/CommentService';
import { CommentPage } from '../comment/comment';
import { Comment } from '../../data/comments';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../../service/AuthService';
import { UserService } from '../../service/UserService';
import { User } from '../../service/user.interface';
import { AngularFireDatabase } from 'angularfire2/database';
import { AlertController, ToastController } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-timeline-detail',
  templateUrl: 'timeline-detail.html',
})
export class TimelineDetailPage {

  public note: any;
  public commentLength: any;
  allComment = [];
  c1:Comment;
  newCommentForm: FormGroup;
  postid:any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public timelineService: TimelineService,
              public commentService: CommentService, public userService: UserService,public authService: AuthService,
              public modalCtrl: ModalController, public db:AngularFireDatabase, public alertCtrl:AlertController, public toastCtrl:ToastController) {
      this.note = this.navParams.get('note');
      this.allComment = this.commentService.show_comment(this.note.id);
      this.commentLength = this.commentService.getCommentLength(this.note.id);
      console.log("comment length" + this.commentLength);
      // console.log("id_post" + this.note.id_post);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TimelineDetailPage');
  }

  ionViewDidLeave(){

  }

  ngOnInit(){
    this.newCommentForm = new FormGroup({
      // name: new FormControl(null, Validators.required),
      comment: new FormControl(null, Validators.required)
    })
  }

  openCommentModal(){
    let modal = this.modalCtrl.create(CommentPage);

    modal.present();
  }

  cancel(){
    this.navCtrl.pop();
  }

  addComment(){
    let newData = this.newCommentForm.value;

    let uemail=this.authService.getCurrentUserEmail();
    let user=this.userService.get_current_user_id_byemail(uemail);
    let username = this.userService.get_user_username(user);
    newData.name = username;

    var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
    let currDate = new Date().toLocaleDateString('en-US', options);

    this.commentService.addComment(this.note.id, username, newData.comment, currDate);
    this.allComment = this.commentService.show_comment(this.note.id);
    this.commentLength = this.commentService.getCommentLength(this.note.id);
    this.newCommentForm.reset();
  }

  openModalDelete(comment){
    const alert = this.alertCtrl.create({
      title: 'Delete Post',
      message: 'Are you sure want to delete this post?',
      cssClass: 'custom-alertDanger',
      buttons: [
        {
          text: 'Yes',
          handler: () => {
            this.commentService.delete_comment(comment.id,this.postid);
            this.allComment = this.timelineService.show_all_post();
            console.log(this.allComment);

            let toast = this.toastCtrl.create({
              message: 'Post has been deleted',
              duration: 2000,
              position: 'bottom'
            });
          
            toast.present();
          }
        },
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'custom-alertDanger',
          handler: () => {
            console.log("Cancel is clicked!");
          }
        }
      ]
    });

    alert.present();
  }

}
