import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';
import { FormGroup, FormControl, Validators } from '../../../node_modules/@angular/forms';
import { NoteService } from '../../service/NoteService';
import { CommentService } from '../../service/CommentService';
import { Note } from '../../data/notes';
import { Comment } from '../../data/comments';
/**
 * Generated class for the CommentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-comment',
  templateUrl: 'comment.html',
})
export class CommentPage {
  newCommentForm: FormGroup;
 
  constructor(public navCtrl: NavController, public navParams: NavParams, private viewCtrl: ViewController, private commentService: CommentService) {
  
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CommentPage');
  }

  addComment(){
    let newData = this.newCommentForm.value;

    console.log(newData.title);

    // this.commentService.addComment(newData.name, newData.comment);

    this.viewCtrl.dismiss();
  }

  ngOnInit(){
    this.newCommentForm = new FormGroup({
      name: new FormControl(null, Validators.required),
      comment: new FormControl(null, Validators.required)
    })
  }

}
