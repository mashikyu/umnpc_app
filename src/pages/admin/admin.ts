import { DailyProblemService } from './../../service/DailyProblemService';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, Platform } from 'ionic-angular';
import { VerifyUserPage } from '../verify-user/verify-user';
import { ViewMemberPage } from '../view-member/view-member';
import { ViewComiteePage } from '../view-comitee/view-comitee';
import { UserService } from '../../service/UserService';
import { AuthService } from '../../service/AuthService';
import { AddSoalPage } from '../add-soal/add-soal';
import { AdminDailyProblemPage } from '../admin-daily-problem/admin-daily-problem';
import { AdminResetProblemPage } from '../admin-reset-problem/admin-reset-problem';
import { LocalNotifications } from '@ionic-native/local-notifications';



@IonicPage()
@Component({
  selector: 'page-admin',
  templateUrl: 'admin.html',
})
export class AdminPage {
  users = [];
  verifyUserPage = VerifyUserPage;
  viewMemberPage = ViewMemberPage;
  viewComiteePage = ViewComiteePage;
  addSoalPage = AddSoalPage;
  adminDailyProblemPage = AdminDailyProblemPage;
  adminResetProblemPage = AdminResetProblemPage;

  constructor(public navCtrl: NavController, public navParams: NavParams, public userService: UserService, public authService: AuthService,public alertCtrl:AlertController,public dailyProblemServ:DailyProblemService , public localNotif:LocalNotifications, public platfrom:Platform) {
    this.users = this.userService.show_all_member();
  }

  ionViewDidLoad() {
   // console.log('ionViewDidLoad AdminPage');
  }

  showPage(page){
    // console.log(this.userService.get_total_member() +" YAY");    
    // var email = this.authService.getCurrentUser();
    this.navCtrl.push(page);

  }

  

  resetDailyProblem(){
    let members=[];
    this.userService.load_all_members();
    members=this.userService.show_all_member();
    const alert = this.alertCtrl.create({
      title: 'Verify Answer',
      message: 'Are you sure ?',
      buttons: [
        {
          text: 'Yes',
          handler: () => {
            ////
            for(let x=0;x<this.userService.get_total_member();x++)
              {
              let id= members[x].id;
             // console.log('ddd= '+id);
              this.dailyProblemServ.update_user_daily_problem(id,'-');
              this.userService.update_user_randomChance(id,'3');
              //console.log('YAY '+this.dailyProblemServ.get_current_daily_problem(id));
              //this.resetSendNews();
              }
            ////
          }
        },
        {
          text: 'No',
          role: 'cancel',
          handler: () => {
            console.log("Cancel is clicked");
          }
        }
      ]
    });

    alert.present();
  }

}
