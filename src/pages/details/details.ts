import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { NoteService } from '../../service/NoteService';
import { Note } from '../../data/notes';

/**
 * Generated class for the DetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-details',
  templateUrl: 'details.html',
})
export class DetailsPage implements OnInit{
  targetNote: Note;

  constructor(public navCtrl: NavController, public navParams: NavParams, private viewCtrl: ViewController, private noteService: NoteService) {
  }

  ngOnInit(){
    this.targetNote = this.navParams.data;
  }
}
