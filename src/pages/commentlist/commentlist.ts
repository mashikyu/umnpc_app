import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ViewController} from 'ionic-angular';
import { FormGroup, FormControl, Validators } from '../../../node_modules/@angular/forms';
import { NoteService } from '../../service/NoteService';
import { CommentService } from '../../service/CommentService';
import { Note } from '../../data/notes';
import { Comment } from '../../data/comments';
import { CommentPage } from '../comment/comment'
/**
 * Generated class for the CommentlistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-commentlist',
  templateUrl: 'commentlist.html',
})
export class CommentlistPage implements OnInit{
  allComment:Comment[] = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, private viewCtrl: ViewController, private commentService: CommentService, public modalCtrl: ModalController) {
  
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CommentlistPage');
  }

  ngOnInit(){
    // this.allComment = this.commentService.fetchAllNotes();
  }

  openCommentModal(){
    let modal = this.modalCtrl.create(CommentPage);

    modal.present();
  }

  cancel(){
    this.viewCtrl.dismiss();
  }
}
