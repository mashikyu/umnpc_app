import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListCommitteePage } from './list-committee';

@NgModule({
  declarations: [
    ListCommitteePage,
  ],
  imports: [
    IonicPageModule.forChild(ListCommitteePage),
  ],
})
export class ListCommitteePageModule {}
