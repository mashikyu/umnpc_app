import { DailyQuizService } from './../../service/QuizService';
import { QuestionService } from './../../service/QuestionService';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { FormGroup, FormControl, Validators } from '@angular/forms';

/**
 * Generated class for the AddSoalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-soal',
  templateUrl: 'add-soal.html',
})
export class AddSoalPage {

  quizForm: FormGroup;

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController,public quizSrv:DailyQuizService) {
    this.quizForm = new FormGroup({
      question: new FormControl(null,Validators.required),
      option_a: new FormControl(null,Validators.required),
      option_b: new FormControl(null,Validators.required),
      option_c: new FormControl(null,Validators.required),
      option_d: new FormControl(null,Validators.required),
      answer: new FormControl(null,Validators.required),
      // angkatan: new FormControl("2014",Validators.required),
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddSoalQuizPage');
  }

  popPage(){
    this.navCtrl.pop();
  }

  submitSoal(){
    console.log(this.quizForm.value);
    const alert = this.alertCtrl.create({
      title: 'Tambah Soal',
      message: 'Soal quiz berhasil ditambahkan',
      buttons:[
        {
          text: 'Ok',
          handler: () => {
            //add soal ke database
            this.quizSrv.add_quiz123(this.quizForm.value);
            this.navCtrl.pop();
          }
        }
      ]
    });

    alert.present();
  }

}
