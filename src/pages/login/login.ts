import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ModalController, AlertController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Md5 } from 'md5-typescript';

import { RegisterPage } from '../../pages/register/register';
import { AuthService } from '../../service/AuthService';
import { UserService } from '../../service/UserService';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  loginForm: FormGroup;

  constructor(public navCtrl: NavController, public navParams: NavParams, public formBuilder: FormBuilder, 
              public authService: AuthService, public loadingCtrl: LoadingController, 
              public modalCtrl: ModalController, public userService: UserService,
              public alertCtrl: AlertController) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  ngOnInit() {
    this.initialize_login_form();
  }

  private initialize_login_form(){
    this.loginForm = new FormGroup({
      email: new FormControl(null, Validators.compose([Validators.maxLength(70), Validators.pattern('^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$'), Validators.required])),
      password: new FormControl(null, Validators.required)
    });
  }

  submit_login()
  {

    let user = this.userService.get_current_user_from_email(this.loginForm.value['email']);

    if(user != null) 
    {
      this.loading_custom();
      this.authService.login(this.loginForm.value['email'], Md5.init(this.loginForm.value['password']));
    }
    else {
      this.userNotFoundAlert();
    }
  }

  submit_register(){
    let modal = this.modalCtrl.create(RegisterPage);
    modal.present();

    modal.onDidDismiss((param) =>{});
  }

  loading_custom(){
    let loading = this.loadingCtrl.create({
      spinner: 'circles',
      content: 'Please Wait...'
    });

    loading.present();

    setTimeout(() => {
      loading.dismiss();
    }, 3000);
  }

  userNotFoundAlert(){
    const alert = this.alertCtrl.create({
      title: 'Error',
      message: 'User Not Found!',
      cssClass: 'custom-alertDanger',
      buttons: [
        {
          text: 'Ok',
          role: 'cancel',
          cssClass: 'custom-alertDanger',
          handler: () => {
            console.log("Cancel is clicked!");
          }
        }
      ]
    });

    alert.present();
  }

}
