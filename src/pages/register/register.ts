import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Md5 } from 'md5-typescript';

import { AuthService } from '../../service/AuthService';
import { UserService } from '../../service/UserService';
/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  registerForm: FormGroup;

  constructor(public navCtrl: NavController, public navParams: NavParams, public formBuilder: FormBuilder, 
              public viewCtrl: ViewController, public authService: AuthService, public userService: UserService,
              public alertCtrl: AlertController) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

  ngOnInit() {
    this.initialize_regis_form();
  }

  private initialize_regis_form() {
    this.registerForm = new FormGroup({
      first_name: new FormControl(null, Validators.required),
      last_name: new FormControl(null, Validators.required),
      email: new FormControl(null, Validators.compose([Validators.maxLength(70), Validators.pattern('^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$'), Validators.required])),
      // cf_handle: new FormControl(null, Validators.required),
      password: new FormControl(null, Validators.required),
      confirm_password: new FormControl(null, Validators.required)
    });
  }

  submit_register() 
  {
    let first_name = this.registerForm.value['first_name'];
    let last_name = this.registerForm.value['last_name'];
    let email = this.registerForm.value['email'];
     let cf_handle = this.registerForm.value['email'];
    let password = this.registerForm.value['password'];
    let confirm_password = this.registerForm.value['confirm_password'];

    if(password != confirm_password) 
    {
      // (front-end) alert password not match
      this.passwordNotMatchAlert();
    } 
    else {
      console.log(this.registerForm.value['first_name'] + " " + this.registerForm.value['last_name']);
      
      this.userService.register(first_name, last_name, email, cf_handle, Md5.init(password));
      this.done_register(this.registerForm.value);  
    }
  }

  done_register(param) 
  {
    this.viewCtrl.dismiss(param);
  }

  close_register_form() 
  {
    this.viewCtrl.dismiss();
  }

  passwordNotMatchAlert(){
    const alert = this.alertCtrl.create({
      title: 'Error',
      message: 'Password Not Match',
      cssClass: 'custom-alertDanger',
      buttons: [
        {
          text: 'Ok',
          role: 'cancel',
          cssClass: 'custom-alertDanger',
          handler: () => {
            console.log("Cancel is clicked!");
          }
        }
      ]
    });

    alert.present();
  }

}
