import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, AlertController } from 'ionic-angular';
import * as moment from 'moment';
import { EventModalCalenderPage } from '../event-modal-calender/event-modal-calender';
/**
 * Generated class for the CalendarPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-calendar',
  templateUrl: 'calendar.html',
})
export class CalendarPage implements OnInit {

  eventSource =[];
  viewTitle:string;

  selectedDay= new Date();
  calendar = {
    mode: 'month',//tingall ganti jadi week ato day
    currentDate: this.selectedDay
  }

  constructor(public navCtrl: NavController, public navParams: NavParams,public modalCtrl:ModalController, private alertCtrl:AlertController) {
  }

  ngOnInit(){
    //masukin data
  }

  addEvent(){
    //
    let modal = this.modalCtrl.create(EventModalCalenderPage, {selectedDay: this.selectedDay});
    modal.present();
    modal.onDidDismiss(data => {
      if (data) {
        let eventData = data;
 
        eventData.startTime = new Date(data.startTime);
        eventData.endTime = new Date(data.endTime);
 
        let events = this.eventSource;
        events.push(eventData);
        this.eventSource = [];
        setTimeout(() => {
          this.eventSource = events;
        });
      }
    });
    //
  }
  onViewTitleChanged(title){
    this.viewTitle=title;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CalendarPage');
  }

  onEventSelected(event){
     let start = moment (event.startTime).format('LLLL');
    let end = moment (event.endTime).format('LLLL');
    let alert = this.alertCtrl.create({
      title:  '<br>delete '+ ' ' + event.title + '?',
      subTitle: 'From: ' + start + '<br>To: ' + end ,
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Yes',
        handler: () => {
          this.navCtrl.push(CalendarPage);
        }
      }
    ]
  });
  alert.present();
    ///
  }

  onTimeSelected(ev){
    this.selectedDay= ev.selectedTime;
  }

}
